"use strict";

const AWS = require('aws-sdk');
const Promise = require( 'bluebird' );
Promise.onPossiblyUnhandledRejection(( err ) => { throw err; });
const kinesis = new AWS.Kinesis({region: 'us-east-1'});




function createStream(shardCount, streamName){
  let params = {
    ShardCount: shardCount,
    StreamName: streamName
  };
  let promises = [];
  promises.push(kinesis.createStream( params ).promise()
    .then(( data ) => {
      //do some stuff
      console.log( 'Successfully created stream ' + streamName +' with ' + shardCount + ' shard(s).');
    })
    .catch(( err ) => {
        //catch and deal with errors
        console.log( err )
    })
  );
  return Promise.all( promises )
};


function deleteStream(streamName){
  let params = {
    StreamName: streamName
  };
  let promises = [];
  promises.push(kinesis.deleteStream(params).promise()
    .then( ( data ) => {
      //do some stuff
      console.log( 'Successfully deleted stream ' + streamName );
    })
    .catch(( err ) => {
        //catch and deal with errors
        console.log( err )
    })
  );
  return Promise.all( promises )
};

function putRecord(data, partitionKey, streamName){
  let params = {
    Data: data,
    PartitionKey: partitionKey,
    StreamName: streamName
  };
  let promises = [];
  promises.push(kinesis.putRecord(params).promise()
    .then( ( data ) => {
      //do some stuff
      console.log( 'Successfully added data to stream ' + streamName );
    })
    .catch(( err ) => {
        //catch and deal with errors
        console.log( err )
    })
  );
  return Promise.all( promises )
};



function describeStream(shardId, shardIteratorType, streamName){
  let params = {
    ShardId: shardId, /* required */
    ShardIteratorType: shardIteratorType, // AT_SEQUENCE_NUMBER | AFTER_SEQUENCE_NUMBER | TRIM_HORIZON | LATEST | AT_TIMESTAMP, /* required */
    StreamName: streamName, /* required */
  };
  let promises = [];
  promises.push(kinesis.getShardIterator(params).promise()
    .then( ( data ) => {
      //do some stuff
      console.log( 'Successfully added data to stream ' + streamName );
    })
    .catch(( err ) => {
        //catch and deal with errors
        console.log( err )
    })
  );
  return Promise.all( promises )
};


function getShardIterator(shardId, shardIteratorType, streamName){ //@TODO do this
  let params = {
    ShardId: shardId, /* required */
    ShardIteratorType: shardIteratorType, // AT_SEQUENCE_NUMBER | AFTER_SEQUENCE_NUMBER | TRIM_HORIZON | LATEST | AT_TIMESTAMP, /* required */
    StreamName: streamName, /* required */
  };
  let promises = [];
  promises.push(kinesis.getShardIterator(params).promise()
    .then( ( data ) => {
      //do some stuff
      console.log( 'Successfully added data to stream ' + streamName );
    })
    .catch(( err ) => {
        //catch and deal with errors
        console.log( err )
    })
  );
  return Promise.all( promises )
};

function getRecords(limit){ //@TODO figure out the shardIterator - not sure how to pass it a shard if I have no way to get the shard.
  let shardIterator = getShardIterator('shardId-000000000000', 'LATEST', 'BaileyTest');
  let params = {
    ShardIterator: shardIterator,
    Limit: limit
  };
  let promises = [];
  promises.push(kinesis.getRecords(params).promise()
    .then( ( data ) => {
      //do some stuff
      console.log( 'Successfully added data to stream ' + streamName );
    })
    .catch(( err ) => {
        //catch and deal with errors
        console.log( err )
    })
  );
  return Promise.all( promises )
};

//createStream(1, 'BaileyTest');
//putRecord('Some data', 'pkey', 'BaileyTest');
getRecords();
